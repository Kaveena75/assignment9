#include<stdio.h>

int main(){

    //decalring a variable to store the file name
    char *filename = "assignment9.txt";

    //opening the file in write mode
    FILE *fptr = fopen(filename, "w" );

   // write to the text file
    fprintf(fptr, "UCSC is one of the leading institutes in Sri Lanka for computing studies.\n");

    // close the file
    fclose(fptr);

    //opening file read mode
    fptr = fopen(filename,"r");

    // read one character at a time and
    // display it to the output
    char ch;
    while ((ch = fgetc(fptr)) != EOF)
        putchar(ch);

    // close the file
    fclose(fptr);

    //opening file append mode
    fptr = fopen(filename, "a");

    // write to the text file
    fprintf(fptr, "UCSC offers undergraduate and postgraduate level courses aiming a range of computing fields.\n");

    // close the file
    fclose(fptr);
}
